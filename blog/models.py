# coding=utf-8
from django.contrib.auth.models import User
from django.db import models
import datetime
from django.forms import ModelForm
from django.utils import timezone


class Publisher(models.Model):
    first_name = models.CharField(max_length=100)
    second_name = models.CharField(max_length=200)
    user = models.OneToOneField(User, default=1)
    profession = models.CharField(max_length=200, default="Not Specified")
    date_naissance = models.DateField(null=True)

    def __unicode__(self):
        return u"%(first_name)s" % {'first_name': self.first_name.upper()}


class Article(models.Model):
    info = models.TextField()
    titre = models.CharField(max_length=200, null=True)
    date_published = models.DateTimeField('date published', auto_now_add=True)
    owner = models.ForeignKey(Publisher, related_name='articles')

    def __unicode__(self):
        """
        the value viewable when someone its querying this objects from an outbreak
        :return:
        """
        return u"owner is %s" % self.owner.first_name

    def was_published_recently(self):
        """
        check if the article was published recently
        :return:
        """
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.date_published < now

    class Meta:
        ordering = ['-date_published']



class Categorie(models.Model):
    titre = models.CharField(max_length=200)
    texte = models.TextField(null=True)
    articles = models.ManyToManyField(Article, related_name="categories", null=True)

    def __unicode__(self):
        """
         the value viewable when someone its querying this objects from an outbreak
        :return:
        :return:
        """
        return u"%s" % self.titre

class Commenter(models.Model):
    user_alias = models.CharField(max_length=250)

    def __unicode__(self):
        """
         the value viewable when someone its querying this objects from an outbreak
        :return:
        :return:
        """
        return u"%s" % self.user_alias


class Comment(models.Model):
    info = models.TextField()
    date_published = models.DateTimeField('date published', auto_now_add=True)
    owner = models.ForeignKey(Commenter, related_name='his_comments')
    on_article = models.ForeignKey(Article, related_name='list_comments')

    def __unicode__(self):
        return u"ceci est le premier %s et ça le second %s" % (self.owner, self.info)






