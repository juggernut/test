# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Categorie'
        db.create_table(u'blog_categorie', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titre', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('texte', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'blog', ['Categorie'])

        # Adding field 'Publisher.profession'
        db.add_column(u'blog_publisher', 'profession',
                      self.gf('django.db.models.fields.CharField')(default='Not Specified', max_length=200),
                      keep_default=False)

        # Adding field 'Publisher.date_naissance'
        db.add_column(u'blog_publisher', 'date_naissance',
                      self.gf('django.db.models.fields.DateField')(null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Categorie'
        db.delete_table(u'blog_categorie')

        # Deleting field 'Publisher.profession'
        db.delete_column(u'blog_publisher', 'profession')

        # Deleting field 'Publisher.date_naissance'
        db.delete_column(u'blog_publisher', 'date_naissance')


    models = {
        u'blog.article': {
            'Meta': {'ordering': "['-date_published']", 'object_name': 'Article'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'articles'", 'to': u"orm['blog.Publisher']"}),
            'titre': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'})
        },
        u'blog.categorie': {
            'Meta': {'object_name': 'Categorie'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texte': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'titre': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'blog.comment': {
            'Meta': {'object_name': 'Comment'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'on_article': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'list_comments'", 'to': u"orm['blog.Article']"}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'default': "'toto'", 'related_name': "'his_comments'", 'to': u"orm['blog.Commenter']"})
        },
        u'blog.commenter': {
            'Meta': {'object_name': 'Commenter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_alias': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'blog.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'date_naissance': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profession': ('django.db.models.fields.CharField', [], {'default': "'Not Specified'", 'max_length': '200'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['blog']