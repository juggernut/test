# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Comment.owner'
        db.add_column(u'blog_comment', 'owner',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='his_comments', null=True, to=orm['blog.Commenter']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Comment.owner'
        db.delete_column(u'blog_comment', 'owner_id')


    models = {
        u'blog.article': {
            'Meta': {'ordering': "['-date_published']", 'object_name': 'Article'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'articles'", 'to': u"orm['blog.Publisher']"}),
            'titre': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'})
        },
        u'blog.comment': {
            'Meta': {'object_name': 'Comment'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'on_article': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'list_comments'", 'to': u"orm['blog.Article']"}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'his_comments'", 'null': 'True', 'to': u"orm['blog.Commenter']"})
        },
        u'blog.commenter': {
            'Meta': {'object_name': 'Commenter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_alias': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'blog.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['blog']