# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Publisher'
        db.create_table(u'blog_publisher', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('second_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'blog', ['Publisher'])

        # Adding model 'Article'
        db.create_table(u'blog_article', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('info', self.gf('django.db.models.fields.TextField')()),
            ('date_published', self.gf('django.db.models.fields.DateTimeField')()),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Publisher'])),
        ))
        db.send_create_signal(u'blog', ['Article'])

        # Adding model 'Commenter'
        db.create_table(u'blog_commenter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_alias', self.gf('django.db.models.fields.CharField')(max_length=250)),
        ))
        db.send_create_signal(u'blog', ['Commenter'])

        # Adding model 'Comment'
        db.create_table(u'blog_comment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('info', self.gf('django.db.models.fields.TextField')()),
            ('date_published', self.gf('django.db.models.fields.DateTimeField')()),
            ('owner', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Commenter'])),
            ('on_article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['blog.Article'])),
        ))
        db.send_create_signal(u'blog', ['Comment'])


    def backwards(self, orm):
        # Deleting model 'Publisher'
        db.delete_table(u'blog_publisher')

        # Deleting model 'Article'
        db.delete_table(u'blog_article')

        # Deleting model 'Commenter'
        db.delete_table(u'blog_commenter')

        # Deleting model 'Comment'
        db.delete_table(u'blog_comment')


    models = {
        u'blog.article': {
            'Meta': {'object_name': 'Article'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Publisher']"})
        },
        u'blog.comment': {
            'Meta': {'object_name': 'Comment'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'on_article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Article']"}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['blog.Commenter']"})
        },
        u'blog.commenter': {
            'Meta': {'object_name': 'Commenter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_alias': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'blog.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['blog']