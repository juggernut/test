# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Article.date_published'
        db.alter_column(u'blog_article', 'date_published', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Comment.date_published'
        db.alter_column(u'blog_comment', 'date_published', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Renaming column for 'Comment.owner' to match new field type.
        db.rename_column(u'blog_comment', 'owner_id', 'owner')
        # Changing field 'Comment.owner'
        db.alter_column(u'blog_comment', 'owner', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))
        # Removing index on 'Comment', fields ['owner']


    def backwards(self, orm):
        # Adding index on 'Comment', fields ['owner']
        db.create_index(u'blog_comment', ['owner_id'])


        # Changing field 'Article.date_published'
        db.alter_column(u'blog_article', 'date_published', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Comment.date_published'
        db.alter_column(u'blog_comment', 'date_published', self.gf('django.db.models.fields.DateTimeField')())

        # User chose to not deal with backwards NULL issues for 'Comment.owner'
        raise RuntimeError("Cannot reverse this migration. 'Comment.owner' and its values cannot be restored.")

    models = {
        u'blog.article': {
            'Meta': {'ordering': "['-date_published']", 'object_name': 'Article'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'articles'", 'to': u"orm['blog.Publisher']"}),
            'titre': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True'})
        },
        u'blog.comment': {
            'Meta': {'object_name': 'Comment'},
            'date_published': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'on_article': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'list_comments'", 'to': u"orm['blog.Article']"}),
            'owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        u'blog.commenter': {
            'Meta': {'object_name': 'Commenter'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_alias': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'blog.publisher': {
            'Meta': {'object_name': 'Publisher'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['blog']