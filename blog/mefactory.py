import factory
from blog import models

__author__ = 'juggernut'


class UserFactory(factory.Factory):
    FACTORY_FOR = models.User
    username = factory.Sequence(lambda n: 'Username{1}'.format(n))
    first_name = 'John'
    last_name = 'Doe'



class PublisherFactory(factory.Factory):
    FACTORY_FOR = models.Publisher
    first_name = factory.Sequence(lambda n: 'UserNr{0}'.format(n))
    second_name = factory.Sequence(lambda n: 'Prenom{1}'.format(n))
    profession = factory.Sequence(lambda n: 'Proffesion{1}'.format(n))
