from django.forms import ModelMultipleChoiceField, CheckboxSelectMultiple
from django.forms.extras import SelectDateWidget
from blog.models import Categorie

__author__ = 'juggernut'

from django import forms


class ArticleForm(forms.Form):
    titre = forms.CharField(max_length=175)
    info = forms.CharField(widget=forms.Textarea())
    mydate = forms.DateField(widget=SelectDateWidget())
    categories = ModelMultipleChoiceField(widget=CheckboxSelectMultiple(), required=False, queryset=Categorie.objects.all())


class CommentForm(forms.Form):
    commenter_alias = forms.CharField(max_length=200)
    info = forms.CharField(widget=forms.Textarea())


