

__author__ = 'juggernut'

from django.conf.urls import patterns, url, include
from blog.views import AboutView, PublisherList
from blog import views
urlpatterns = patterns('',
                       (r'^about/', AboutView.as_view()),
                       (r'^publishers/', PublisherList.as_view()),
                       url(r'^$', views.index, name='index'),
                       url(r'^article/(?P<article_id>\d+)/$', views.page_article, name='article'),
                       url(r'^categorie/(?P<categorie_id>\d+)/$', views.categorie, name='categorie'),
                       url(r'^login/$', 'django.contrib.auth.views.login', {
                           'template_name': 'blog/login.html',
                       }, name='login'),
                       (r'^logout/$', 'django.contrib.auth.views.logout', {
                           'next_page': '/blog/'
                       }),
                       url(r'^new_article/$', views.new_article, name='new_article'),


)
