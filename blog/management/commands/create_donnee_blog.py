# coding=utf-8
__author__ = 'juggernut'
from django.core.management.base import BaseCommand, CommandError
from blog.models import Article, Publisher, Comment, Commenter


class Command(BaseCommand):
    def handle(self, *args, **options):
        p = Publisher.objects.get_or_create(
            first_name="Paul",
            second_name="Guichon"
        )[0]
        a = Article.objects.get_or_create(
            titre="exemple de texte",
            info="""
            StarCraft est un jeu vidéo de stratégie en temps réel (STR) développé par Blizzard Entertainment. La version PC du jeu fonctionnant sous Microsoft Windows sort le 31 mars 1998 et s’inscrit dans la droite lignée des deux premiers succès de Blizzard Entertainment dans le domaine des jeux de stratégie en temps réel : Warcraft: Orcs & Humans et Warcraft II: Tides of Darkness. Avec plus de onze millions de copies vendues dans le monde, il est l’un des jeux vidéo sur PC les mieux vendus et reste à ce jour le jeu de stratégie en temps réel le plus vendu de tous les temps.
En janvier 1999, la sortie de l’extension StarCraft: Brood War également développée par Blizzard Entertainment précède les sorties des versions Macintosh et Nintendo 64 qui ont lieu respectivement en mars 1999 et en juin 2000. Une suite est publiée le 27 juillet 2010 sous le nom de StarCraft II: Wings of Liberty, et rencontre également un grand succès commercial avec plus de 4,5 millions de copies vendues six mois après sa sortie.
Le jeu prend place au 26e siècle et relate les affrontements entre trois espèces distinctes, pour la domination d’une zone de la Voie lactée connue sous le nom de Secteur Koprulu. Les Terrans sont constitués de descendants de bagnards terriens exilés loin de leur monde natal alors que les Zergs sont une race d’insectes modifiés génétiquement et obsédés par l’assimilation des autres espèces de la Galaxie. Enfin, les Protoss constituent une race d’humanoïdes disposant de technologies et de pouvoirs psychiques très avancés et cherchant à préserver leur civilisation de la menace des Zergs. StarCraft est aujourd’hui considéré comme un pionnier du genre pour avoir incorporé trois races aussi distinctes dans un jeu de stratégie temps réel.
De nombreux journalistes spécialisés désignent StarCraft comme un des meilleurs et des plus importants jeux vidéo de tous les temps, considérant que celui-ci a mis la barre très haut dans le genre des jeux de stratégie en temps réel. Le mode multijoueur de StarCraft: Brood War est particulièrement populaire, notamment en Corée du Sud où de nombreux joueurs ont acquis le statut de professionnels et participent à des compétitions dont les matchs sont retransmis à la télévision.
            """,
            owner=p
        )
        a2 = Article.objects.get_or_create(
            titre="exemple de texte numero 2",
            info="""
            StarCraft est un jeu vidéo de stratégie en temps réel (STR) développé par Blizzard Entertainment. La version PC du jeu fonctionnant sous Microsoft Windows sort le 31 mars 1998 et s’inscrit dans la droite lignée des deux premiers succès de Blizzard Entertainment dans le domaine des jeux de stratégie en temps réel : Warcraft: Orcs & Humans et Warcraft II: Tides of Darkness. Avec plus de onze millions de copies vendues dans le monde, il est l’un des jeux vidéo sur PC les mieux vendus et reste à ce jour le jeu de stratégie en temps réel le plus vendu de tous les temps.
En janvier 1999, la sortie de l’extension StarCraft: Brood War également développée par Blizzard Entertainment précède les sorties des versions Macintosh et Nintendo 64 qui ont lieu respectivement en mars 1999 et en juin 2000. Une suite est publiée le 27 juillet 2010 sous le nom de StarCraft II: Wings of Liberty, et rencontre également un grand succès commercial avec plus de 4,5 millions de copies vendues six mois après sa sortie.
Le jeu prend place au 26e siècle et relate les affrontements entre trois espèces distinctes, pour la domination d’une zone de la Voie lactée connue sous le nom de Secteur Koprulu. Les Terrans sont constitués de descendants de bagnards terriens exilés loin de leur monde natal alors que les Zergs sont une race d’insectes modifiés génétiquement et obsédés par l’assimilation des autres espèces de la Galaxie. Enfin, les Protoss constituent une race d’humanoïdes disposant de technologies et de pouvoirs psychiques très avancés et cherchant à préserver leur civilisation de la menace des Zergs. StarCraft est aujourd’hui considéré comme un pionnier du genre pour avoir incorporé trois races aussi distinctes dans un jeu de stratégie temps réel.
De nombreux journalistes spécialisés désignent StarCraft comme un des meilleurs et des plus importants jeux vidéo de tous les temps, considérant que celui-ci a mis la barre très haut dans le genre des jeux de stratégie en temps réel. Le mode multijoueur de StarCraft: Brood War est particulièrement populaire, notamment en Corée du Sud où de nombreux joueurs ont acquis le statut de professionnels et participent à des compétitions dont les matchs sont retransmis à la télévision.
            """,
            owner=p
        )




