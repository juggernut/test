__author__ = 'juggernut'
from django.contrib import admin
from blog.models import Article, Commenter, Comment, Publisher, Categorie

admin.site.register(Article)
admin.site.register(Commenter)
admin.site.register(Comment)
admin.site.register(Publisher)
admin.site.register(Categorie)