# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.utils import timezone
from django.views.generic import TemplateView, ListView
from blog.forms import ArticleForm, CommentForm
from blog.models import Article, Commenter, Comment, Categorie, Publisher
from django.shortcuts import render, get_object_or_404, render_to_response


class AboutView(TemplateView):
    template_name = "blog/about.html"


class PublisherList(ListView):
    model = Publisher
    template_name = "blog/publishers_list.html"

def index(request):
    article_List = Article.objects.filter(date_published__lte=timezone.now())
    categories = Categorie.objects.all()
    return render_to_response('blog/index.html',
                              {'article_List': article_List,
                              'categories': categories},
                              context_instance=RequestContext(request))



def categorie(request, categorie_id=None):
    article_List = Article.objects.all()
    categories = Categorie.objects.all()
    if categorie_id is None:
        categorie_id = categories[0].id
    else:
        tmpCat = get_object_or_404(Categorie, pk=categorie_id)
        article_List = tmpCat.articles.all()
    return render_to_response('blog/categorie.html',
                              {'article_List': article_List,
                               'categories': categories,
                               'selected_categorie': int(categorie_id)},
                              context_instance=RequestContext(request))


def page_article(request, article_id):
    article = get_object_or_404(Article, pk=article_id)
    if request.method == 'POST': # If the form has been submitted...
        form = CommentForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            commenter,created = Commenter.objects.get_or_create(user_alias=form.cleaned_data['commenter_alias'])
            info = form.cleaned_data['info']
            comment = Comment(info=info, owner=commenter, on_article=article)
            comment.save()
            stringUrl = '/blog/article/' + article_id + '/'
            return HttpResponseRedirect(stringUrl) # Redirect after POST
    else:
        form = CommentForm() # An unbound form
    return render(request, 'blog/article.html', {"article": article, "form": form}, )


@login_required
def new_article(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ArticleForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            titre = form.cleaned_data['titre']
            info = form.cleaned_data['info']
            categories = form.cleaned_data['categories']

            published_date = form.cleaned_data['mydate']
            # published_date = datetime.datetime(published_date.year, published_date.month, published_date.day)
            print(categories)
            publisher = request.user.publisher
            article = Article(info=info, titre=titre, owner=publisher, date_published=published_date)
            article.save()
            article.categories = categories
            article.save()
            return HttpResponseRedirect('/blog/')  # Redirect after POST
    else:
        form = ArticleForm() # An unbound form

    return render(request, 'blog/new_article.html', {
        'form': form,
        })


def new_comment(request):
    if request.method == 'POST': # If the form has been submitted...
        form = ArticleForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            titre = form.cleaned_data['titre']
            info = form.cleaned_data['info']
            publisher = request.user.publisher
            article = Article(info=info, titre=titre, owner=publisher)
            article.save()
            return HttpResponseRedirect('/blog/') # Redirect after POST
    else:
        form = ArticleForm() # An unbound form

    return render(request, 'blog/new_article.html', {
        'form': form,
        })

# def logout_view(request):
#     print "coucou"
#     logout(request)
#
#     return HttpResponseRedirect(reverse('index'))

# def testCredentials(request):
#     username = request.POST['username']
#     password = request.POST['password']
#     print(request)
#     user = authenticate(username=username, password=password)
#     if user is not None:
#         if user.is_active:
#             login(request, user)
#             #index(request)
#             return redirect('index')
#         else:
#             return HttpResponse("Huge credentials error.")
#     else:
#         return HttpResponse("User NONE.")